<?php
namespace Drupal\tfw_stations\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class StationController extends ControllerBase {

  /**
   * Get station details.
   *
   * @return array
   */
  public function tfw_stations_station() {
    // Connect to NRE API..
    $token = tfw_stations_nre_connection();

    // Get all stations from NRE API.
    if($token != '') {
      tfw_stations_nre_get_stations($token);
    }

    // Get station name from URL.
    /*$path  = explode('/',\Drupal::request()->getpathInfo());
    $station_name  = $path[2];
    dpm($station_name);*/

    // INDIVIDUAL STATION DETAILS.
    /*$ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt($ch, CURLOPT_URL, 'http://int.web.kb.awsnre.co.uk/xml/30/station-ACY.xml');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    file_put_contents('sites/tfw.gov.wales/files/NRE/nre_stations_individual.xml',$output);
    //dpm($output);
    curl_close($ch);*/

    // Return data.
    $build = [
      '#markup' => $this->t('Hello World!'),
    ];

    // Return.
    return $build;
  }
}
